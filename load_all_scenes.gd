@tool
extends EditorScript

func _run():
	var nodes = get_scene().get_children()
	var count = nodes.size()
	var square = round(sqrt(count))
	
	var x = 0
	var y = 0
	var offset = 7
	
	for node in nodes:
		node.position = Vector3(x * offset ,0, (-1) * y * offset)
		x = x + 1
		
		if x == square:
			y = y + 1
			x = 0
