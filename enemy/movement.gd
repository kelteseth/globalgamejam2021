extends Node

@export var MOVEMENT_SPEED = 10
@export var ROTATION_SPEED = 7

var target_pos = null
var player_pos = null

var patrol_route = []
var current_patrol_index = 0

@onready var parent = get_parent()

func _ready():
	for patrol_node in get_node('../PatrolPoints').get_children():
		self.patrol_route.append(patrol_node.global_transform.origin)

func _physics_process(delta):
	var distance = (self.patrol_route[self.current_patrol_index] - get_parent().global_transform.origin).length()
	if distance < 1:
		self.current_patrol_index += 1
	
	if current_patrol_index >= patrol_route.size():
		current_patrol_index = 0

	target_pos = self.patrol_route[self.current_patrol_index]
	
	if player_pos:
		target_pos = player_pos
		if (get_parent().global_transform.origin - player_pos).length() < 1:
			player_pos = null

	var target_dir = (target_pos - parent.position).normalized()
	target_dir.y = 0
	var look_at_target = parent.position + target_dir
	var new_transform = parent.transform.looking_at(look_at_target, Vector3.UP)
	parent.transform = parent.transform.interpolate_with(new_transform, delta * ROTATION_SPEED)
	
	parent.move_and_collide(target_dir * MOVEMENT_SPEED * delta + Vector3.DOWN * delta * 4, true)
