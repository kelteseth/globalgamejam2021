extends Node3D

var possibly_visible = []


func _ready():
	get_node("../SightCheckTimer").paused = true


func _on_DetectionArea_body_entered(body):
	get_node("../SightCheckTimer").paused = false
	
	self.possibly_visible.append(body)


func _on_DetectionArea_body_exited(body):
	self.possibly_visible.erase(body)
	
	if self.possibly_visible.size() == 0:
		get_node("../SightCheckTimer").paused = true


func _on_SightCheckTimer_timeout():
	for node in self.possibly_visible:
		var space_state = get_world_3d().direct_space_state
		var result = space_state.intersect_ray(get_parent().position, node.position)
		
		if result:
			if (result.position - node.position).length() < 1.0:
				get_node('../Movement').player_pos = result.position
