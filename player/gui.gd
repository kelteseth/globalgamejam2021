extends Control


func show_pickup(visible):
	get_node("pickup").visible = visible


func _on_Button_pressed():
	get_node("death").visible = false
	get_node("/root/Node3D/room_manager").load_main_menu()

func show_need_loot():
	get_node("need_loot").visible = true
	get_node("need_loot/Timer").start()

func _on_Timer_timeout():
	get_node("need_loot").visible = false

func show_score():
	get_node("win").visible = true
