extends Area3D

@export var KICK_FORCE = 10

var nearby_bodies = []

func _on_InteractArea_body_entered(body):
	self.nearby_bodies.append(body)

func _on_InteractArea_body_exited(body):
	self.nearby_bodies.erase(body)

func _input(event):
	if event.is_action_pressed("kick"):
		if self.nearby_bodies.size() > 0:
			var closest_distance = 1000
			var closest_kickable = null
			for body in self.nearby_bodies:
				var kickable = body.get_node_or_null('IKickable')
				if kickable:
					var distance = (body.global_transform.origin - self.global_transform.origin).length()
					
					if distance < closest_distance:
						closest_distance = distance
						closest_kickable = kickable
			
			if closest_kickable:
				closest_kickable.get_kicked(self.global_transform.origin, KICK_FORCE)
