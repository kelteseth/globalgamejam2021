extends CharacterBody3D

@export var player_model: NodePath

var rotation_speed = 10
var movement_speed = 3
var interact = false
var rotation_deg = 180
var running_factor = 0

var movement_up: float
var movement_right: float
var movement_down: float
var movement_left: float

var roation_speed = 15.0

var rotation_up: float
var rotation_right: float
var rotation_down: float
var rotation_left: float
var gravity = -9.81

var player_ref
var pickup_object
var collection_count = 0

var has_loot = false
var gui

# Called when the node enters the scene tree for the first time.
func _ready():
	self.player_ref = get_node(player_model)
	self.gui = get_node("gui")

func _input(event):
	self.movement_up = Input.get_action_strength("player_movement_up")
	self.movement_right = Input.get_action_strength("player_movement_right")
	self.movement_down = Input.get_action_strength("player_movement_down")
	self.movement_left = Input.get_action_strength("player_movement_left")
	self.interact = Input.get_action_strength("interact")
	
	if self.interact:
		if is_instance_valid(self.pickup_object):
			collection_count += 1
			self.gui.get_node("collection_count/RichTextLabel").set_text(str(collection_count))
			self.pickup_object.pickup()

			self.has_loot = true

func _physics_process(delta):
	var speed_vertical = ( self.movement_up - self.movement_down) * self.movement_speed
	var speed_horizontal = (  self.movement_right - self.movement_left) * self.movement_speed
	var velocity = Vector3(speed_vertical,  (gravity),  speed_horizontal).rotated(Vector3(0, 1, 0), deg_to_rad(45))

	self.set_velocity(velocity)
	self.set_up_direction(Vector3.UP)
	self.set_floor_stop_on_slope_enabled(true)
	self.move_and_slide()
	self.velocity
	
	velocity.y = 0
	if velocity.length() > 0.1:
		var new_transform = player_ref.transform.looking_at(player_ref.transform.origin + velocity, Vector3.UP)
		player_ref.transform = player_ref.transform.interpolate_with(new_transform, roation_speed * delta)
		
		
	# Walk animation
	var movement_vertical = (self.movement_up - self.movement_down)
	var movement_horizontal = (self.movement_right - self.movement_left)
	var input = Vector2(movement_horizontal, movement_vertical)
	var run_percentage = input.length()
	if run_percentage > 1.0:
		run_percentage = 1.0
	var animated_character = get_node("Node3D/AnimatedCharacter/AnimationTree")
	var prev_run_percentage = animated_character.get('parameters/walk/blend_amount')
	#animated_character.set('parameters/walk/blend_amount', lerp(prev_run_percentage, run_percentage, 0.2))


func death():
	self.gui.get_node("death").visible = true
