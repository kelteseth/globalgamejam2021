extends Node

@onready var box = get_parent()

func get_kicked(force_origin, force):
	var direction = (box.global_transform.origin - force_origin).normalized()
	direction += Vector3.UP * 1
	
	box.apply_central_impulse(direction * force)
	get_node("../break_sound").play()
	box.invincible = true
	get_node("../Particles").emitting = true
	

	box.get_node('DestructTimeout').start()
