extends RigidBody3D

var invincible = false
var destroy_on_impact = false

func _on_Box_body_entered(body):
	if (self.linear_velocity.length() > 5 and not invincible) or self.destroy_on_impact:
		get_node('/root/Node3D/Node3D/DistractionManager').emit_signal('distraction', self.global_transform.origin)
		self.queue_free()


func _on_DestructTimeout_timeout():
	self.invincible = false
	self.destroy_on_impact = true
