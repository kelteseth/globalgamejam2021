extends Node3D


func set_enabled(enabled):
	var rigid_body = get_node("RigidBody3D")
	if enabled:
		rigid_body.set_axis_lock(PhysicsServer3D.BODY_AXIS_LINEAR_X, false)
		rigid_body.set_axis_lock(PhysicsServer3D.BODY_AXIS_LINEAR_Z, false)
		rigid_body.set_axis_lock(PhysicsServer3D.BODY_AXIS_ANGULAR_Y , false)
		#rigid_body.mode = RigidBody3D.MODE_RIGID
	else:
		rigid_body.set_axis_lock(PhysicsServer3D.BODY_AXIS_LINEAR_X, true)
		rigid_body.set_axis_lock(PhysicsServer3D.BODY_AXIS_LINEAR_Z, true)
		rigid_body.set_axis_lock(PhysicsServer3D.BODY_AXIS_ANGULAR_Y , true)
		#rigid_body.mode = RigidBody3D.FREEZE_MODE_STATIC 


func _on_Area_body_entered(body):
	if body.name == "player":
		set_enabled(get_node("/root/Node3D/player").has_loot)
