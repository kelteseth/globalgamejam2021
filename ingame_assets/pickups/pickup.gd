extends Node


func _on_Area_body_entered(body):
	if body.name == 'player':
		body.get_node("gui").show_pickup(true)
		body.pickup_object = self


func _on_Area_body_exited(body):
	if body.name == 'player':
		body.get_node("gui").show_pickup(false)
		body.pickup_object = null

func pickup():
	if !get_node("Timer").is_stopped():
		return
	get_node("Timer").start()
	get_node("Particles").restart()
	get_node("AudioStreamPlayer").play(.2)
	dissolve()


func _on_Timer_timeout():
	queue_free()


func dissolve():
	return
	var tween = get_node('Tween')
	tween.interpolate_method(
		self, "animate_dissolve", 0, 1, 1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT
	)
	tween.start()

func animate_dissolve(progress: float) -> void:
	var materials = []
	materials.append(get_node("chestTop_commongltf/chestTop_common").get_surface_override_material(0))
	materials.append(get_node("chestTop_commongltf/chestTop_common").get_surface_override_material(1))
	
	materials.append(get_node("chest_commongltf/chest_common").get_surface_override_material(0))
	materials.append(get_node("chest_commongltf/chest_common").get_surface_override_material(1))
	materials.append(get_node("chest_commongltf/chest_common").get_surface_override_material(2))
	materials.append(get_node("chest_commongltf/chest_common").get_surface_override_material(3))
	
	for material in materials:
		material.set_shader_parameter("dissolve_amount", ease(progress, 0.4))
	
	get_node("OmniLight3D").light_energy = get_node("OmniLight3D").light_energy * (1 - progress)
