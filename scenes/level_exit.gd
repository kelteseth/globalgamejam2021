extends Node3D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal exit

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area_body_entered(body):
	if body.name == 'player':
		if !body.has_loot:
			body.get_node("gui").show_need_loot()
			return
		get_node("Timer").start()

# hack because we now have the same collider for level
# switch and door interaction
func _on_Timer_timeout():
	emit_signal("exit")
