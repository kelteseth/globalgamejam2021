extends AudioStreamPlayer

#@export var samples # (Array, AudioStreamWAV)

func _ready():
	randomize()
	play_random_sample()

func _on_Sampler_finished():
	play_random_sample()

func play_random_sample():
	#var random_index = randi()%samples.size()-1
	#self.stream = samples[random_index]
	self.play()
