extends Node3D

var level
var level_exit
var player
var room_index = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	connect_level_exit()

func connect_level_exit():
	self.level_exit = get_node_or_null("/root/Node3D/Node3D/level_exit")
	self.level = get_node_or_null("/root/Node3D/Node3D")
	
	if !is_instance_valid(self.level_exit):
		print("error get level_exit node Node3D")
		return
		
	self.level_exit.connect("exit", Callable(self, "exit_signal_handler"))


func exit_signal_handler():
	self.level_exit.disconnect("exit", Callable(self, "exit_signal_handler"))
	self.room_index += 1
	if self.room_index == 3:
		self.player.get_node("gui").show_score()
	self.level.connect("tree_exited", Callable(self, "level_unload"))
	self.level.queue_free()

func level_unload():
	var next_level_resource = load("res://scenes/room_0" + str(self.room_index) + ".tscn")
	var next_level = next_level_resource.instantiate()
	get_node("/root/Node3D").add_child(next_level)
	connect_level_exit()
	self.player.position = get_level_start_pos()
	self.player.has_loot = false

func load_crypt():
	var next_level_resource = load("res://scenes/crypt.tscn")
	var next_level = next_level_resource.instantiate()
	get_node("/root/Node3D").add_child(next_level)
	self.player.position = get_level_start_pos()
	connect_level_exit()
	self.room_index = 0
	self.player.has_loot = false

func load_main_menu():
	self.level.connect("tree_exited", Callable(self, "load_crypt"))
	self.level.queue_free()

func get_level_start_pos():
	return get_node("/root/Node3D/Node3D/level_start").position
	
func start():
	get_node("../Camera3D").queue_free()
	var scene = load("res://player/player.tscn")
	self.player = scene.instantiate()
	self.player.position = get_level_start_pos()
	get_node("/root/Node3D").add_child(player)
	get_node("../character_rogue").queue_free()
	get_node("../skullgltf").queue_free()
	
